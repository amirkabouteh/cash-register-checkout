﻿using System;
using System.Collections.Generic;
using System.Xml;
//using System.Linq;
using System.Text;
using CoreScanner;
using Cometd.Client;
using Cometd.Client.Transport;
using Cometd.Bayeux;
using Cometd.Bayeux.Client;
using Cometd.Common;


namespace BarCodeScanner
{
    class Listener : IMessageListener
    {
        public void onMessage(IClientSessionChannel channel, IMessage message)
        {
            Console.WriteLine(message);
        }
    }

    class Program
    {
        // Declare CoreScannerClass
        static CCoreScannerClass cCoreScannerClass;
        static string URL = "http://192.168.0.78:6500/faye";
        static BayeuxClient myClient;
        static IClientSessionChannel myChannel;


        
        
        public static byte[] FromHex(string hex)  // translating the Hex output value of scanner to string
        {
            hex = hex.Replace("0x", "");
            hex = hex.Replace(" ", "");
            byte[] raw = new byte[hex.Length / 2];
            for (int i = 0; i < raw.Length; i++)
            {
                raw[i] = Convert.ToByte(hex.Substring(i * 2, 2), 16);
            }
            return raw;
        }

        static void OnBarcodeEvent(short eventType, ref string pscanData)
        {
            // extracting barcode number from the scanner output 
            XmlDocument Input = new XmlDocument();
            Input.LoadXml(pscanData);
            XmlNodeList element = Input.GetElementsByTagName("datalabel");
            Console.WriteLine(pscanData);

            // translating the Hex value from the scanner to normal string value
            byte[] data = FromHex(element[0].InnerText);
            string s = Encoding.ASCII.GetString(data);
            Console.WriteLine("Scannner Event! Scan Data: " + s);
            
            // sending the message to the channel
            SendMessage(s);
        }

        static void SendMessage(string message)  // sending message to the subscribed channel
        {
            myChannel.publish(message);
        }
        
        
        static void Main(string[] args)
        {


            //Scanner instantiation
            cCoreScannerClass = new CCoreScannerClass();
            short[] scannerTypes = new short[1];                // Scanner Types you are interested in
            scannerTypes[0] = 1;                                // 1 for all scanner types
            short numberOfScannerTypes = 1;                     // Size of the scannerTypes array
            int status;                                         // Extended API return code

            // Faye Client variable instantiation
            myClient = new BayeuxClient(URL, new List<ClientTransport> { new LongPollingTransport(null) });
            myClient.handshake();
            myClient.waitFor(1000, new List<BayeuxClient.State>() { BayeuxClient.State.CONNECTED });
            myChannel = myClient.getChannel("/private/read/abcdefghijklmnopqrstuvwxyz");
            myChannel.subscribe(new Listener());
            
            
            
            
            // open connection to the scanner
            cCoreScannerClass.Open(0, scannerTypes, numberOfScannerTypes, out status);
            
            // check if the connection was successful
            if (status == 0)
            {
                Console.WriteLine("CoreScanner API: Open Successful");
            }
            else
            {
                Console.WriteLine("CoreScanner API: Open Failed");
            }


  

            // Lists all scanners connected to the host computer.
            short numberOfScanners;
            int[] connectedScannerIDList = new int[255];
            string outXML;
            cCoreScannerClass.GetScanners(out numberOfScanners, connectedScannerIDList, out outXML, out status);
            Console.WriteLine(outXML);

            
            int opcode = 1001;
            string inXML = "<inArgs>" +
                "<cmdArgs>" +
                "<arg-int>1</arg-int>" +
                "<arg-int>1</arg-int>" +
                "</cmdArgs>" +
                "</inArgs>";
            cCoreScannerClass.ExecCommand(opcode, ref inXML, out outXML, out status);

            opcode = 2011;
            inXML = "<inArgs>" +
               "<scannerID>1</scannerID>" +
                  "</inArgs>";
            cCoreScannerClass.ExecCommand(opcode, ref inXML, out outXML, out status);
            cCoreScannerClass.BarcodeEvent += new _ICoreScannerEvents_BarcodeEventEventHandler(OnBarcodeEvent);
            Console.ReadKey(); 

            
            // close faye connection to the server
            myClient.disconnect();
            myClient.waitFor(1000, new List<BayeuxClient.State>() { BayeuxClient.State.DISCONNECTED });


            


        }
            
        
    }
}
